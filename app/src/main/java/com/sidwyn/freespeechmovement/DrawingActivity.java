package com.sidwyn.freespeechmovement;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

import com.gmail.yuyang226.flickrj.sample.android.FlickrjActivity;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.googlecode.flickrjandroid.photos.Size;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SVBar;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by sidwyn on 10/11/14.
 */
public class DrawingActivity extends Activity implements OnClickListener, ColorPicker.OnColorChangedListener{
    File fileUri;

    private static int UPLOAD_PHOTO_OPERATION = 100;
    private static int GET_PHOTO_OPERATION = 101;

    //custom drawing view
    private DrawingView drawView;
    //buttons
    private ImageButton currPaint, drawBtn, eraseBtn, newBtn, saveBtn;
    //sizes
    private float smallBrush, mediumBrush, largeBrush;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.drawingactivity);
        Intent intent = getIntent();
        String toBeTitle = intent.getStringExtra("title");
        if (toBeTitle.length() > 0) {
            toBeTitle = toBeTitle.replace("\\", "");
            Log.e("tobetitle", toBeTitle);
            setTitle(toBeTitle);
        }
//get drawing view
        drawView = (DrawingView)findViewById(R.id.drawing);

        //get the palette and first color button
//        LinearLayout paintLayout = (LinearLayout)findViewById(R.id.paint_colors);
//        currPaint = (ImageButton)paintLayout.getChildAt(0);
//        currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));

        //sizes from dimensions
        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        //draw button
        drawBtn = (ImageButton)findViewById(R.id.draw_btn);
        drawBtn.setOnClickListener(this);

        //set initial size
        drawView.setBrushSize(mediumBrush);

        //erase button
        eraseBtn = (ImageButton)findViewById(R.id.erase_btn);
        eraseBtn.setOnClickListener(this);

        //new button
        newBtn = (ImageButton)findViewById(R.id.new_btn);
        newBtn.setOnClickListener(this);

        //save button
        saveBtn = (ImageButton)findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);

        ColorPicker picker = (ColorPicker) findViewById(R.id.picker);
        SVBar svBar = (SVBar) findViewById(R.id.svbar);

        picker.addSVBar(svBar);

        //To get the color
        picker.getColor();

        //To set the old selected color u can do it like this
        picker.setOldCenterColor(picker.getColor());

        picker.setShowOldCenterColor(false);

        picker.setOnColorChangedListener(this);
    }

    @Override
    public void onColorChanged(int i) {

        drawView.setErase(false);
        drawView.setBrushSize(drawView.getLastBrushSize());
        drawView.setColorInt(i);
    }

    //user clicked paint
    public void paintClicked(View view){
        //use chosen color

        //set erase false
        drawView.setErase(false);
        drawView.setBrushSize(drawView.getLastBrushSize());

        if(view!=currPaint){
            ImageButton imgView = (ImageButton)view;
            String color = view.getTag().toString();
            drawView.setColor(color);
            //update ui
            imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
            currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint));
            currPaint=(ImageButton)view;
        }
    }



    @Override
    public void onClick(View view){

        if(view.getId()==R.id.draw_btn){
            //draw button clicked
            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle("Brush size:");
            brushDialog.setContentView(R.layout.brush_chooser);
            //listen for clicks on size buttons
            ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(false);
                    drawView.setBrushSize(smallBrush);
                    drawView.setLastBrushSize(smallBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(false);
                    drawView.setBrushSize(mediumBrush);
                    drawView.setLastBrushSize(mediumBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(false);
                    drawView.setBrushSize(largeBrush);
                    drawView.setLastBrushSize(largeBrush);
                    brushDialog.dismiss();
                }
            });
            //show and wait for user interaction
            brushDialog.show();
        }
        else if(view.getId()==R.id.erase_btn){
            //switch to erase - choose size
            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle("Eraser size:");
            brushDialog.setContentView(R.layout.brush_chooser);
            //size buttons
            ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(smallBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(mediumBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(largeBrush);
                    brushDialog.dismiss();
                }
            });
            brushDialog.show();
        }
        else if(view.getId()==R.id.new_btn){
            //new button
            AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
            newDialog.setTitle("New drawing");
            newDialog.setMessage("Start new drawing (you will lose the current drawing)?");
            newDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    drawView.startNew();
                    dialog.dismiss();
                }
            });
            newDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.cancel();
                }
            });
            newDialog.show();
        }
        else {
            if (view.getId() == R.id.save_btn) {
                //save drawing
                drawView.setDrawingCacheEnabled(true);
                //attempt to save
                Bitmap bitmap = drawView.getDrawingCache();
                bitmap = scaleCenterCrop(bitmap, 250, 288);

                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

                File mypath=new File(directory,"flickr_upload.jpg");
                try {
                    if (!mypath.exists()) {
                        mypath.createNewFile();
                    }
                    FileOutputStream ostream = new FileOutputStream(mypath);
                    bitmap.compress(CompressFormat.PNG, 10, ostream);
                    ostream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                uploadFlickrPhoto();

//            String imgSaved = MediaStore.Images.Media.insertImage(
//                    getContentResolver(), drawView.getDrawingCache(),
//                    path + "/sdcard/flickr_upload.png", "drawing");


//            //feedback
//            if(imgSaved!=null){
//                Toast savedToast = Toast.makeText(getApplicationContext(),
//                        "Drawing saved to Gallery!", Toast.LENGTH_SHORT);
//                savedToast.show();
//                uploadFlickrPhoto();
//            }
//            else{
//                Toast unsavedToast = Toast.makeText(getApplicationContext(),
//                        "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
//                unsavedToast.show();
//            }
                drawView.destroyDrawingCache();
            }
        }
    }

    // FLICKR

    protected void uploadFlickrPhoto() {

        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        File mypath=new File(directory,"flickr_upload.jpg");
        try
        {
            Intent intent = new Intent(getApplicationContext(), FlickrjActivity.class);
            intent.putExtra("flickImagePath", mypath.getAbsolutePath());
            System.out.println("Calling upload photo");
            startActivityForResult(intent, UPLOAD_PHOTO_OPERATION);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }finally
        {

//            mv.setDrawingCacheEnabled(false);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("I'm coming back from an intent!");
        if (requestCode == GET_PHOTO_OPERATION) {
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("Return");



//                ((ImageView) findViewById(R.id.imview))
//                        .setImageURI(tmp_fileUri);

//                String selectedImagePath = getPath(tmp_fileUri);
//                fileUri = new File(selectedImagePath);
            }

        }
        else if (requestCode == UPLOAD_PHOTO_OPERATION) {
            System.out.println("Upload ");
            showImage();
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("Upload complete");
            }

        }
    };

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void showImage() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    System.out.println("Running show image");
                    String svr="www.flickr.com";

                    REST rest=new REST();
                    rest.setHost(svr);

                    //initialize Flickr object with key and rest
                    Flickr flickr=new Flickr(com.gmail.yuyang226.flickrj.sample.android.FlickrHelper.API_KEY,rest);

                    //initialize SearchParameter object, this object stores the search keyword
                    SearchParameters searchParams=new SearchParameters();
                    searchParams.setSort(SearchParameters.INTERESTINGNESS_DESC);

                    //Create tag keyword array
                    String[] tags=new String[]{"cs160fsm"};
                    searchParams.setTags(tags);

                    //Initialize PhotosInterface object
                    PhotosInterface photosInterface=flickr.getPhotosInterface();
                    //Execute search with entered tags
                    PhotoList photoList=photosInterface.search(searchParams,20,1);
                    Log.w("I got back photolist", "here");
                    System.out.println(photoList.size());
                    //get search result and fetch the photo object and get small square imag's url
                    if(photoList!=null){
                        Log.w("Going in", "");
                        //Get search result and check the size of photo result
                        Random random = new Random();
                        int seed = random.nextInt(photoList.size());
                        //get photo object
                        Photo photo = (Photo)photoList.get(seed);
                        PhotosInterface pi = flickr.getPhotosInterface();
                        InputStream is = pi.getImageAsStream(photo, Size.MEDIUM);
                        //Get small square url photo
//                        InputStream is = photo.getImageAsStream();
                        Bitmap bm = BitmapFactory.decodeStream(is);
                        bm = scaleCenterCrop(bm, 250, 288);
//                        Uri tmp_fileUri = data.getData();
                        try {
//                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), tmp_fileUri);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();

                            Intent in1 = new Intent();
                            in1.putExtra("bitmap",byteArray);

                            setResult(Activity.RESULT_OK, in1);
                            Log.w("Calling Main Activity", "");
                            finish();
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (FlickrException e) {
                    e.printStackTrace();
                } catch (IOException e ) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }
    private Bitmap returnBitmapNoScale(int resource) {
        BitmapFactory.Options mNoScale = new BitmapFactory.Options();
        mNoScale.inScaled = false;
        return BitmapFactory.decodeResource(getResources(), resource, mNoScale);
    }

    private Bitmap scaleCenterCrop(Bitmap source, int newWidth, int newHeight) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.drawBitmap(source, null, targetRect, null);

        return scaledBitmap;
    }
}
