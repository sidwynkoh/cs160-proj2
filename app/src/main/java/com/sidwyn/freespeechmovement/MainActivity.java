package com.sidwyn.freespeechmovement;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;

import java.io.InputStream;


public class MainActivity extends Activity {

    private DeckOfCardsManagerListener deckOfCardsManagerListener;
    private DeckOfCardsManager mDeckOfCardsManager;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private DeckOfCardsEventListener deckOfCardsEventListener;

    private CardImage[] cardImages;

    private static final int DRAWING_ACTIVITY = 100;

    private boolean shownNotification = false;
    private long notifTimeStamp = 0; // In seconds

    private LocationManager locationManager;
    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Free Speech Movement");

        LinearLayout myGallery = myGallery = (LinearLayout)findViewById(R.id.mygallery);

        try {
            myGallery.addView(insertPhoto(getBitmap("art_goldberg_scroll.png")));
            myGallery.addView(insertPhoto(getBitmap("jack_weinberg_scroll.png")));
            myGallery.addView(insertPhoto(getBitmap("jackie_goldberg_scroll.png")));
            myGallery.addView(insertPhoto(getBitmap("joan_baez_scroll.png")));
            myGallery.addView(insertPhoto(getBitmap("mario_savio_scroll.png")));
            myGallery.addView(insertPhoto(getBitmap("michael_rossman_scroll.png")));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                Location sproulPlaza = new Location("sproulPlaza");
                sproulPlaza.setLatitude(37.86965);
                sproulPlaza.setLongitude(-122.25914);
                Log.e("Current distance from sproul plaza", Float.toString(sproulPlaza.distanceTo(location)));
                if (sproulPlaza.distanceTo(location) < 50) {
                    Log.e(Long.toString(System.currentTimeMillis()), "currentTime");
                    if (!shownNotification) {
                        shownNotification = true;
                        sendNotification();
                        notifTimeStamp = System.currentTimeMillis();
                    }
                    else if (shownNotification && (System.currentTimeMillis() - notifTimeStamp) > 60000) { // It's been more than 60 seconds since last notification
                        shownNotification = true;
                        sendNotification();
                        notifTimeStamp = System.currentTimeMillis();
                    }
                }
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_SHORT).show();
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
        }

        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        deckOfCardsEventListener= new DeckOfCardsEventListenerImpl();
        deckOfCardsManagerListener= new DeckOfCardsManagerListenerImpl();
        mDeckOfCardsManager.addDeckOfCardsManagerListener(deckOfCardsManagerListener);
        mDeckOfCardsManager.addDeckOfCardsEventListener(deckOfCardsEventListener);

        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }

        findViewById(R.id.show_drawing_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Opening drawing activity");
                // Open drawing activity

                Intent intent = new Intent(MainActivity.this, DrawingActivity.class);
                intent.putExtra("title", "Draw Something!");
                startActivityForResult(intent, DRAWING_ACTIVITY);
            }
        });

        findViewById(R.id.view_wikipedia_article).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Opening wikipedia");

                Intent i = new Intent("android.intent.action.MAIN");
                i.setComponent(ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main"));
                i.addCategory("android.intent.category.LAUNCHER");
                i.setData(Uri.parse("https://en.wikipedia.org/wiki/Free_Speech_Movement"));
                startActivity(i);
            }
        });


    }

    private void install() {
        updateDeckOfCardsFromUI(6);
        try{
            if (mDeckOfCardsManager.isInstalled()) {
                Log.e("WTF", "ALREADY INSTALLED");
            }
            else {
                Log.e("WTF", "NOT INSTALLED");
                mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);

            }
        }
        catch (RemoteDeckOfCardsException e){
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    private void updateDeckOfCardsFromUI(int number) {
        if (mRemoteDeckOfCards == null) {
            mRemoteDeckOfCards = createDeckOfCards(number);
        }

        ListCard listCard= mRemoteDeckOfCards.getListCard();
        // Card #1
        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(0);
        simpleTextCard.setHeaderText("Art Goldberg");
        simpleTextCard.setTitleText("Draw 'Now'");
        simpleTextCard.setCardImage(mRemoteResourceStore, cardImages[0]);
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setShowDivider(true);

        // Card #2
        SimpleTextCard simpleTextCard2= (SimpleTextCard)listCard.childAtIndex(1);
        simpleTextCard2.setHeaderText("Jack Weinberg");
        simpleTextCard2.setTitleText("Draw 'FSM'");
        simpleTextCard2.setCardImage(mRemoteResourceStore, cardImages[1]);
        simpleTextCard2.setReceivingEvents(true);
        simpleTextCard2.setShowDivider(true);

        // Card #3
        SimpleTextCard simpleTextCard3= (SimpleTextCard)listCard.childAtIndex(2);
        simpleTextCard3.setHeaderText("Jackie Goldberg");
        simpleTextCard3.setTitleText("Draw 'SLATE'");
        simpleTextCard3.setCardImage(mRemoteResourceStore, cardImages[2]);
        simpleTextCard3.setReceivingEvents(true);
        simpleTextCard3.setShowDivider(true);

        // Card #4
        SimpleTextCard simpleTextCard4= (SimpleTextCard)listCard.childAtIndex(3);
        simpleTextCard4.setHeaderText("Joan Baez");
        simpleTextCard4.setTitleText("Draw a Megaphone");
        simpleTextCard4.setCardImage(mRemoteResourceStore, cardImages[3]);
        simpleTextCard4.setReceivingEvents(true);
        simpleTextCard.setShowDivider(true);

        // Card #5
        SimpleTextCard simpleTextCard5= (SimpleTextCard)listCard.childAtIndex(4);
        simpleTextCard5.setHeaderText("Mario Savio");
        simpleTextCard5.setTitleText("Express your own view of Free Speech in an drawing");
        simpleTextCard5.setCardImage(mRemoteResourceStore, cardImages[4]);
        simpleTextCard5.setReceivingEvents(true);
        simpleTextCard5.setShowDivider(true);

        // Card #6
        SimpleTextCard simpleTextCard6= (SimpleTextCard)listCard.childAtIndex(5);
        simpleTextCard6.setHeaderText("Michael Rossman");
        simpleTextCard6.setTitleText("Draw 'Free Speech'");
        simpleTextCard6.setCardImage(mRemoteResourceStore, cardImages[5]);
        simpleTextCard6.setReceivingEvents(true);
        simpleTextCard6.setShowDivider(true);

    }

    private void updateDeckOfCardsFromUIWithExtra() {
        updateDeckOfCardsFromUI(7);
        ListCard listCard= mRemoteDeckOfCards.getListCard();

        // Card #7
        SimpleTextCard simpleTextCard7= (SimpleTextCard)listCard.childAtIndex(6);
        simpleTextCard7.setHeaderText("Random Image");
        simpleTextCard7.setTitleText("Tap to check it out!");
        simpleTextCard7.setCardImage(mRemoteResourceStore, cardImages[6]);
        simpleTextCard7.setReceivingEvents(false);
        simpleTextCard7.setShowDivider(true);
    }
    //
    // Create some cards with example content
    private RemoteDeckOfCards createDeckOfCards(int number){
        ListCard listCard= new ListCard();
        for (int i = 0; i < number; i++) {
            String toConcatenate = "card" + Integer.toString(i);
            SimpleTextCard simpleTextCard= new SimpleTextCard(toConcatenate);
            listCard.add(simpleTextCard);
        }
        return new RemoteDeckOfCards(this, listCard);
    }

    //    Initialise
    private void init(){
        // Create the resource store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();
        // Get the card images
        try{
            cardImages= new CardImage[7];
            cardImages[0]= new CardImage("card.image.1", getBitmap("art_goldberg_toq.png"));
            cardImages[1]= new CardImage("card.image.2", getBitmap("jack_weinberg_toq.png"));
            cardImages[2]= new CardImage("card.image.3", getBitmap("jackie_goldberg_toq.png"));
            cardImages[3]= new CardImage("card.image.4", getBitmap("joan_baez_toq.png"));
            cardImages[4]= new CardImage("card.image.5", getBitmap("mario_savio_toq.png"));
            cardImages[5]= new CardImage("card.image.6", getBitmap("michael_rossman_toq.png"));

        }
        catch (Exception e){
            Toast.makeText(this, getString(R.string.error_initialising_card_images), Toast.LENGTH_SHORT).show();
            Log.e(Constants.TAG, "ToqApiDemo.init - error occurred parsing the card images", e);
            return;
        }

        mRemoteResourceStore.addResource(cardImages[0]);
        mRemoteResourceStore.addResource(cardImages[1]);
        mRemoteResourceStore.addResource(cardImages[2]);
        mRemoteResourceStore.addResource(cardImages[3]);
        mRemoteResourceStore.addResource(cardImages[4]);
        mRemoteResourceStore.addResource(cardImages[5]);
        // Try to retrieve a stored deck of cards
        try {
            mRemoteDeckOfCards = createDeckOfCards(6);
        }
        catch (Throwable th){
            th.printStackTrace();
        }
    }

    private void sendNotification() {
        String[] message = new String[1];
        message[0] = "Go to the FSM app to view more!";
        // Create a NotificationTextCard
        NotificationTextCard notificationCard = new NotificationTextCard(System.currentTimeMillis(),
                "You're on Sproul!", message);

        // Draw divider between lines of text
        notificationCard.setShowDivider(true);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);

        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification(notification);
//            Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Failed to send Notification", Toast.LENGTH_SHORT).show();
        }
    }

    // Read an image from assets and return as a bitmap
    private Bitmap getBitmap(String fileName) throws Exception{
        System.out.println("fileName");
        System.out.println(fileName);
        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }
    /**
     * @see android.app.Activity#onStart()
     */
    protected void onStart(){
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onStop(){
        super.onStop();
        Log.d(Constants.TAG, "ToqApiDemo.onStop");
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
//        mDeckOfCardsManager.removeDeckOfCardsEventListener(deckOfCardsEventListener);
    }


    // Handle service connection lifecycle and installation events
    private class DeckOfCardsManagerListenerImpl implements DeckOfCardsManagerListener{

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener#onConnected()
         */
        public void onConnected(){
            runOnUiThread(new Runnable(){
                public void run(){
                    Log.e("Connected!", "Yahoo!");
                    init();
                    install();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener#onDisconnected()
         */
        public void onDisconnected(){
            runOnUiThread(new Runnable(){
                public void run(){
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener#onInstallationSuccessful()
         */
        public void onInstallationSuccessful(){
            runOnUiThread(new Runnable(){
                public void run(){
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener#onInstallationDenied()
         */
        public void onInstallationDenied(){
            runOnUiThread(new Runnable(){
                public void run(){
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener#onUninstalled()
         */
        public void onUninstalled(){
            runOnUiThread(new Runnable(){
                public void run(){
                }
            });
        }

    }

    // Handle card events triggered by the user interacting with a card in the installed deck of cards
    private class DeckOfCardsEventListenerImpl implements DeckOfCardsEventListener{

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardOpen(java.lang.String)
         */
        public void onCardOpen(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    System.out.println("Card open");
//                    Toast.makeText(getApplicationContext(), getString(R.string.event_card_open) + cardId, Toast.LENGTH_SHORT).show();
                    // Open drawing activity

                    Intent intent = new Intent(MainActivity.this, DrawingActivity.class);
                    ListCard listCard= mRemoteDeckOfCards.getListCard();
                    SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(Integer.parseInt(cardId.substring(4)));
                    Log.e("simpletextcard title", simpleTextCard.getTitleText());
                    intent.putExtra("title", simpleTextCard.getTitleText());
                    startActivityForResult(intent, DRAWING_ACTIVITY);
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardVisible(java.lang.String)
         */
        public void onCardVisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    System.out.println("Card visible");
//                    Toast.makeText(getApplicationContext(), getString(R.string.event_card_visible) + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardInvisible(java.lang.String)
         */
        public void onCardInvisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
//                    Toast.makeText(getApplicationContext(), getString(R.string.event_card_invisible) + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardClosed(java.lang.String)
         */
        public void onCardClosed(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    System.out.println("Card closed");
//                    Toast.makeText(getApplicationContext(), getString(R.string.event_card_closed) + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(java.lang.String, java.lang.String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption){
            runOnUiThread(new Runnable(){
                public void run(){
//                    Toast.makeText(getApplicationContext(), getString(R.string.event_menu_option_selected) + cardId + " [" + menuOption + "]", Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(java.lang.String, java.lang.String, java.lang.String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption, final String quickReplyOption){
            runOnUiThread(new Runnable(){
                public void run(){
//                    Toast.makeText(getApplicationContext(), getString(R.string.event_menu_option_selected) + cardId + " [" + menuOption + ":" + quickReplyOption +
//                            "]", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


    // Callback from child activity

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Callback to main activity");
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (100) : {
                System.out.println("adding new random image");
                if (resultCode == Activity.RESULT_OK) {
                    init();

                    byte[] byteArray = data.getByteArrayExtra("bitmap");
                    Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

                    try{
                        cardImages[6]= new CardImage("card.image.7", bmp);

                    }
                    catch (Exception e){
                        Toast.makeText(this, getString(R.string.error_initialising_card_images), Toast.LENGTH_SHORT).show();
                        Log.e(Constants.TAG, "ToqApiDemo.init - error occurred parsing the card images", e);
                        return;
                    }

                    try {
                        mRemoteResourceStore.addResource(cardImages[6]);
                        mRemoteDeckOfCards = createDeckOfCards(7);
                        updateDeckOfCardsFromUIWithExtra();
                        mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
                        System.out.println("Updating cards with random");
//                        deckOfCardsEventListener= new DeckOfCardsEventListenerImpl();
//                        mDeckOfCardsManager.addDeckOfCardsEventListener(deckOfCardsEventListener);
                    }
                    catch (RemoteDeckOfCardsException e){
                        e.printStackTrace();
                        Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();
                    }
                    catch (Throwable th){
                        th.printStackTrace();
                    }

                }
                break;
            }
        }
    }

    // Image Helper Methods

    View insertPhoto(Bitmap path){
//        Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

        Bitmap bm = path;
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setLayoutParams(new LayoutParams(280, 340));
        layout.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setLayoutParams(new LayoutParams(250, 340));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bm);

        layout.addView(imageView);
        return layout;
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            } else {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }

        return inSampleSize;
    }
}
